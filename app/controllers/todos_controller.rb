class TodosController < ApplicationController

  #   Returns a view page for All Todo Records stores in Database
  #   @@Route: GET: /todos
  #   @@params
  #   @@return
  def index
    @todos = Todo.all
  end

  #   Returns a view page for a Todo Record stores in Database that matches with given id
  #   @@Route: GET: /todos/:id
  #   @@params: :id
  #   @@return
  def show
    @todo = Todo.find(params[:id])
  end

  #   Returns a view page for creating new Todo Record
  #   @@Route: GET: /todos/new
  #   @@params
  #   @@return
  def new
    @todo = Todo.new
  end

  #   Returns a view page for editing a Todo Record that matches with given id
  #   @@Route: GET: /todos/:id/edit
  #   @@params: :id
  #   @@return
  def edit
    @todo = Todo.find(params[:id])
  end

  #   Creates a new Todo Record
  #   @@Route: POST: /todos
  #   @@params: Todo{:title, :details}
  #   @@return
  def create
    # initializing new Todo object with allowed form data
    @todo = Todo.new(todo_create_params)
    # saving the newly instantiated todo object to database
    if @todo.save
      # Successfully saved to database. Now redirecting to view page of newly created Todo
      redirect_to @todo
    else
      # !!!Validation Error occurred. Redirecting back with validation message and input.
      render 'new'
    end
  end

  #   Updates a Todo Record that matches with the given object
  #   @@Route: PATCH: /todos/:id
  #   @@params: Todo{:id, :title, :details, :status}
  #   @@return
  def update
    # Getting a Todo Record that matches with the given id
    @todo = Todo.find(params[:id])
    # Updating the current version of todo object with allowed data
    if @todo.update(todo_update_params)
      # Successfully updated to database. Now redirecting to view page of just updated Todo
      redirect_to @todo
    else
      # !!!Validation Error occurred. Redirecting back with validation message and input.
      render 'edit'
    end
  end

  #   Deletes a Todo Record that matches with the given id
  #   @@Route: Delete: /todos/:id
  #   @@params: :id
  #   @@return
  def destroy
    # Getting a Todo Record that matches with the given id
    @todo = Todo.find(params[:id])
    # Deleting the todo record matched with given id
    @todo.destroy
    redirect_to todos_path
  end


  # Private Block
  # All Helper Functions go here

  private

  #   Returns attributes that are valid for creating new Todo
  #   @@returns: permitted Todo Object
  def todo_create_params
    params.require(:todo).permit(:title, :details)
  end

  #   Returns attributes that are valid for updating existing Todo
  #   @@returns: permitted Todo Object
  def todo_update_params
    params.require(:todo).permit(:title, :details, :status)
  end
end
