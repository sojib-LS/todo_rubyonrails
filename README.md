# Todo App 

A simple todo app built with Ruby on Rails. This application is developed for getting a 
better understanding of CRUD operation in Ruby on Rails framework with MySQL

## Getting Started

Before cloning this application, it is advised to go through with these resources to get a
better understanding of the programming language Ruby and framework Ruby on Rails:
 
* [ProgrammingRuby](http://docs.ruby-doc.com/docs/ProgrammingRuby/) - The pragmatic textbook of Ruby
* [Learn Ruby the hard way](https://learnrubythehardway.org/book/) - The fun and quick way to learn Ruby 
* [Learn Ruby in Twenty Minutes](https://www.ruby-lang.org/en/documentation/quickstart/) - The quickest way to learn ruby. 
* [Learn Ruby from other language](https://www.ruby-lang.org/en/documentation/ruby-from-other-languages/) - The shortcut to Ruby.
 Not recommended if you have no prior knowledge of programming language.
 
 The resource below can be helpful to get a quick grasp of Ruby on Rails framework  

* [Getting Started with Ruby on Rails](https://guides.rubyonrails.org/getting_started.html) - A step by step detailed implementation of CRUD operations in Ruby on Rails with basic architecture of the framework.

### Installing

Follow these resources to install Ruby and Ruby on Rails on your computer

Resource for installing Ruby

* [Official Installation Guide of Ruby](https://www.ruby-lang.org/en/documentation/installation/)

Resource for installing Ruby on Rails

* [Official Installation Guide of Ruby On Rails](https://guides.rubyonrails.org/v5.0/getting_started.html#installing-rails)

## After Cloning this repository



1. Go to the newly cloned app directory
    ```
    cd todo_app
    ```
2. Make sure your mysql server is up and running on port `3306`
3. Copy `database.yml.example` file in `config` folder and paste the file as `database.yml` in the same directory.

    1. In windows:
        ```
       copy config\database.yml.example config\database.yml  
        ```

    2. In linux(ubuntu):
       ```
        cp config/database.yml.example config/database.yml  
       ```

4. Open the `database.yml` file and change the default section of database configuration as your local mysql configuration.
    ```
    default: &default
        adapter: mysql2
        encoding: utf8
        pool: <%= ENV.fetch("RAILS_MAX_THREADS") { 5 } %>
        username: root
        password:
        host: localhost
    ``` 

5. Execute the following command to create development and test databases: 

    ```
    rake db:create
    ```

6. Execute the following command to perform database migration: 

    ```
    rails db:migrate
    ```
    
7. And now, the final step is to start the server with the following command:

    ```
    rails server
    ```

## Built With

This application was built by `rails new app` command. For more information visit [this](https://guides.rubyonrails.org/command_line.html#rails-new) link. 


## Authors

* **Ahashan Alam Sojib** - *Software Engineer* - [Linkstaff Co Ltd](https://linkstaff.co.jp)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Ruby on Rails Guide
* Stackoverflow
* etc
