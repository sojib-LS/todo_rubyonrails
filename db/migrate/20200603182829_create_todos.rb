class CreateTodos < ActiveRecord::Migration[6.0]
  def change
    create_table :todos do |t|
      t.string :title, null: false
      t.text :details
      t.boolean :status, default: false

      t.timestamps
    end
  end
end
